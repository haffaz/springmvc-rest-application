<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  ~ (C) Copyright 2006-2007 hSenid Software International (Pvt) Limited.
  ~ All Rights Reserved.
  ~
  ~ These materials are unpublished, proprietary, confidential source code of
  ~ hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
  ~ of hSenid Software International (Pvt) Limited.
  ~
  ~ hSenid Software International (Pvt) Limited retains all title to and intellectual
  ~ property rights in these materials.
  ~
  --%>

<%--
  Created by IntelliJ IDEA.
  User: mohomed
  Date: 7/25/18
  Time: 4:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Register</title>
</head>
<body>
<div class="container">

    <!-- nav bar -->
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills float-right">
                <li class="nav-item">
                    <a class="nav-link active mr-sm-2" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <form class="form-inline mr-sm-2" action="/getUser">
                        <input class="form-control mr-sm-2" type="search" name="id" placeholder="Search by id"
                               aria-label="Search">
                        <button class="btn btn-outline-success" id="btn_search" type="submit">Search</button>
                    </form>
                </li>
            </ul>
        </nav>
        <h3 class="text-muted">Spring MVC</h3>
    </div>

    <div class="alert alert-success alert-dismissible collapse" role="alert" id="success-register">
        <strong>Successfully registered user !</strong>
    </div>

    <div class="alert alert-danger alert-dismissible collapse" role="alert" id="error-register">
        <strong>Error !</strong>
    </div>

    <form id="userForm">

        <div class="form-group">
            <label for="input_name">Name</label>
            <input type="text" class="form-control" id="input_name" placeholder="Name" required>
        </div>

        <div class="form-group">
            <label for="input_age">Age</label>
            <input type="number" class="form-control" id="input_age" placeholder="28">
        </div>

        <div class="form-group">
            <label for="input_address">Address</label>
            <input type="text" class="form-control" id="input_address" placeholder="1234 Main St" required>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="input_user_name">User Name</label>
                <input type="text" class="form-control" id="input_user_name" placeholder="User Name" required>
            </div>
            <div class="form-group col-md-6">
                <label for="input_password">Password</label>
                <input type="password" class="form-control" id="input_password" placeholder="Password" required>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Register</button>
    </form>
</div>


<!-- bootstrap and jquery scripts -->
<!-- bootstrap and jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<!-- ajax -->
<script src="<c:url value="/resources/js/ToggleAlerts.js"/>"></script>
<script src="<c:url value="/resources/js/CreateUser.js"/>"></script>
<script src="<c:url value="/resources/js/UpdateUser.js"/>"></script>

</body>
</html>
