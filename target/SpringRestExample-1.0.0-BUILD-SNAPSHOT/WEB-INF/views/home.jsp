<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%--
  ~ (C) Copyright 2006-2007 hSenid Software International (Pvt) Limited.
  ~ All Rights Reserved.
  ~
  ~ These materials are unpublished, proprietary, confidential source code of
  ~ hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
  ~ of hSenid Software International (Pvt) Limited.
  ~
  ~ hSenid Software International (Pvt) Limited retains all title to and intellectual
  ~ property rights in these materials.
  ~
  --%>

<html>

<head>
    <title>User Management System</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>


<div class="container">
    <!-- nav bar -->
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills float-right">
                <li class="nav-item">
                    <a class="nav-link active mr-sm-2" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <div class="form-inline mr-sm-2">
                        <input class="form-control mr-sm-2" type="search" id="search_text" name="id"
                               placeholder="Search by id"
                               aria-label="Search">
                        <button class="btn btn-outline-success"
                                onclick="ajaxSearch(document.getElementById('search_text').value)">Search
                        </button>
                    </div>
                </li>
                <%--  <li class="nav-item">
                      <a class="nav-link" href="#">Contact</a>
                  </li>--%>
            </ul>
        </nav>
        <h3 class="text-muted">Spring MVC</h3>
    </div>

    <div class="jumbotron">
        <h1 class="display-3">SPRING WEB APPLICATION</h1>
        <p class="lead">A simple web application using spring mvc, hibernate and sql</p>
        <p><a class="btn btn-lg btn-success" href="/register" role="button">Sign up now</a></p>
    </div>

    <div class="alert alert-success alert-dismissible collapse" role="alert" id="delete-user-alert">
        <strong>Successfully deleted user !</strong>
    </div>

    <div class="alert alert-danger alert-dismissible collapse" role="alert" id="no-user-alert">
        <strong>No users !</strong>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <button id="btn_all" class="btn btn-secondary my-2 my-sm-0">Hide Users</button>

            <div class="col-lg-10 center-block">

                <div id="getResultDiv">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Name</th>
                            <th scope="col">Age</th>
                            <th scope="col">Address</th>
                            <th scope="col">User Name</th>
                            <th scope="col">Password</th>
                        </tr>
                        </thead>
                        <tbody id="user_info">

                        </tbody>

                    </table>
                </div>
            </div>
        </div>

        <footer class="footer">
            <p>&copy; HMS 2017</p>
        </footer>

    </div> <!-- /container -->
</div>

<!-- bootstrap and jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<!--ajax -->
<script src="<c:url value="/resources/js/ToggleAlerts.js"/>"></script>
<script src="<c:url value="/resources/js/DeleteUser.js"/>"></script>
<script src="<c:url value="/resources/js/GetAllUsers.js"/>"></script>
<script src="<c:url value="/resources/js/SearchUser.js"/>"></script>
</body>

</html>
