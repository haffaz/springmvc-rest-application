/*
 * (C) Copyright 2006-2007 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 *
 */

function ajaxDelete(id) {
    $.ajax({
        type: "DELETE",
        url: window.location + "/user/delete/" + id,
        // dataType: 'json',
        success: function (result) {
            //User deleted successfully
            ajaxGet();
            $(".alert-success").show();
            toggleAlert("#delete-user-alert");
            console.log("SUCCESS: ", result);
        },
        error: function (e) {
            $("#getResultDiv").html("<strong>Error</strong>");
            console.log("ERROR: ", e);
        }
    });
}

/*function toggleAlert(){
    window.setTimeout(function() {
        $('.alert-success').fadeTo(1000, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 4000);
}*/

