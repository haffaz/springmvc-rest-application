/*
 * (C) Copyright 2006-2007 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 *
 */


// DO GET
function ajaxSearch(id){
    $.ajax({
        type : "GET",
        url : window.location + "user/" + id,
        dataType: 'json',
        success: function(result){
            $('#getResultDiv ul').empty();
            $('#user_info').empty();

            var $tr = $('<tr>').append(
                $('<td>').text(result.id),
                $('<td>').text(result.name),
                $('<td>').text(result.age),
                $('<td>').text(result.address),
                $('<td>').text(result.user_name),
                $('<td>').text(result.password),
                $('<td>').innerHTML = '<button class="btn btn-primary" onclick="edit('+id+')">Edit</button>\n' +
                    '<button class="btn btn-danger" onclick="ajaxDelete('+id+')">Delete</button>\n' );
            $('#getResultDiv #user_info').append($tr);
            console.log("Success: ", result);
        },
        error : function(e) {
            $('#getResultDiv').hide();
            $('#user_info').empty();
            $(".alert-danger").show();
            toggleAlert("#no-user-alerts");
            console.log("ERROR: ", e);
        }
    });
}

function edit(id) {
    window.location = "/update/?id=" + id;
}

/*function toggleAlert(){
    window.setTimeout(function() {
        $("#no-user-alert").fadeTo(1000, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 4000);
}*/

$( document ).ready(function() {
    //event.preventDefault();
    ajaxSearch(id);
})

