function toggleAlert(alert_id){
    window.setTimeout(function() {
        $(alert_id).fadeTo(1000, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 4000);
}