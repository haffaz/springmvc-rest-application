/*
 * (C) Copyright 2006-2007 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 *
 */

package com.hms.spring.controller;

public class UserRestURIConstants {

	public static final String DUMMY_USER   = "/user/dummy";
	public static final String GET_USER     = "/user/{id}";
	public static final String GET_ALL_USER = "/users";
	public static final String CREATE_USER  = "/register/user/create";
	public static final String EDIT_USER    = "/user/edit/{id}";
	public static final String DELETE_USER  = "/user/delete/{id}";
}
