/*
 * (C) Copyright 2006-2007 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 *
 */

package com.hms.spring.controller;

import com.hms.spring.model.User;
import com.hms.spring.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Handles requests for the User service.
 */
@RestController
public class Controller {

    private static final Logger logger = LoggerFactory.getLogger(Controller.class);


    @Autowired
    private UserService userService;


    @RequestMapping(value = UserRestURIConstants.DUMMY_USER, method = RequestMethod.GET)
    public @ResponseBody
    User getDummyData() {
        logger.info("Start getDummyData");
        return new User(1, "dummy", 20, "city", "dummy", "123");
    }

    @RequestMapping(value = UserRestURIConstants.GET_USER, method = RequestMethod.GET)
    public @ResponseBody
    User getUser(@PathVariable("id") int id) {
        logger.info("Start getUser. ID=" + id);
        return userService.getUser(id);
    }

    @RequestMapping(value = UserRestURIConstants.GET_ALL_USER,
            method = RequestMethod.GET)
    public @ResponseBody
    List<User> getAllUsers() {
        logger.info("Start getAllUsers.");
        return userService.getAllUsers();
    }

    @RequestMapping(value = UserRestURIConstants.CREATE_USER,
            method = RequestMethod.POST)
    public @ResponseBody
    User addUser(@RequestBody User user) {
        //User user = new User(name, age, address, user_name, password);
        logger.info("Start getUser.");
        userService.addUser(user);
        return user;
    }

    @RequestMapping(value = UserRestURIConstants.EDIT_USER,
            method = RequestMethod.PUT)
    public @ResponseBody
    User editUser(@PathVariable("id") int id,
                  @RequestBody User user) {
        logger.info("Start editUser.");
        user.setId(id);
        userService.updateUser(user);
        return user;
    }

    @RequestMapping(value = UserRestURIConstants.DELETE_USER,
            method = RequestMethod.DELETE)
    public @ResponseBody
    User deleteUser(@PathVariable("id") int id) {
        logger.info("Start deleteUser.");
        User user = userService.getUser(id);
        userService.deleteUser(id);
        return user;
    }

}
