package com.hms.spring.dao.impl.mysql;

/*
 * (C) Copyright 2006-2007 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

import com.hms.spring.dao.UserDao;
import com.hms.spring.model.User;
import com.hms.spring.model.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public void addUser(User user) {
        String sql = "insert into user(name,age,address,user_name,password) "
                + "values (?, ?, ?, ?, ?)";
        jdbcTemplateObject.update(sql, user.getName(), user.getAge(), user.getAddress(),
                user.getUser_name(), user.getPassword());
    }

    @Override
    public List<User> getAllUsers() {
        String sql = "select * from user";
        List<User> users = jdbcTemplateObject.query(sql, new UserMapper());
        return users;
    }

    @Override
    public void deleteUser(Integer id) {
        String sql = "delete from user where id = ?";
        jdbcTemplateObject.update(sql, id);
    }

    @Override
    public User updateUser(User user) {
        String sql = "update user set name=?, age=?, address=?, user_name=?, password=? where id=?";

        jdbcTemplateObject.update(sql, user.getName(), user.getAge(), user.getAddress(),
                user.getUser_name(), user.getPassword(), user.getId());

        return getUser(user.getId());
    }


    @Override
    public User getUser(Integer id) {
        String sql = "select * from user where id = ?";
        User user = null;
        try {
            user = jdbcTemplateObject.queryForObject(sql, new Object[]{id}, new UserMapper());
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return user;
    }
}

