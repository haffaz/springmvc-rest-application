/*
 * (C) Copyright 2006-2007 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 *
 */

package com.hms.spring.dao;


import com.hms.spring.model.User;

import java.util.List;

/**
 * this interface contains methods that provides access to
 * the mysql database
 */
public interface UserDao {

    /**
     * inserts user into the database
     *
     * @param user
     */
    public void addUser(User user);

    /**
     * gets all the users from the database
     *
     * @return : all users
     */
    public List<User> getAllUsers();

    /**
     * deletes user of a given id from the
     * database
     *
     * @param id
     */
    public void deleteUser(Integer id);

    /**
     * updates details of an existing user in
     * the database
     *
     * @param user
     * @return
     */
    public User updateUser(User user);

    /**
     * gets users details of a given id from the
     * database
     *
     * @param id : user id
     * @return : user
     */
    public User getUser(Integer id);
}
