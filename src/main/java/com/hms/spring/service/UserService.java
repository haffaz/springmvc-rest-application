/*
 * (C) Copyright 2006-2007 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 *
 */

package com.hms.spring.service;

import com.hms.spring.model.User;

import java.util.List;

/**
 * This interface sends client requests to the
 * dao layer and returns responses
 */
public interface UserService {

    /**
     * gets the user from the client
     * and forwards to the doa class
     *
     * @param user
     */
    void addUser(User user);

    /**
     * returns the list of users from the doa
     * layer when a client requests
     *
     * @return : list of users
     */
    List<User> getAllUsers();

    /**
     * directs the client request to delete
     * a user to the doa class
     *
     * @param id : user id
     */
    void deleteUser(Integer id);

    /**
     * directs the clients request to
     * update a users details to the doa
     * layer
     *
     * @param user : updated user
     * @return : updated user
     */
    User updateUser(User user);

    /**
     * returns a user when a client makes a
     * request
     *
     * @param id : user id
     * @return : user
     */
    User getUser(int id);
}
