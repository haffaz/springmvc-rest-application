/*
 * (C) Copyright 2006-2007 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 *
 */

package com.hms.spring;

import com.hms.spring.dao.UserDao;
import com.hms.spring.model.User;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


//@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
//@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml"})
public class TestUserService {

    private static User testUser;
    @Autowired
    private UserDao userDao;

    @BeforeClass
    @Ignore
    public static void initUser() {
        testUser = new User("test", 20, "city", "test", "123");
    }

    @Test
    @Ignore
    public void testAddUser() {
        int size = userDao.getAllUsers().size();
        userDao.addUser(testUser);
        Assert.assertTrue(userDao.getAllUsers().size() > size);
    }

    @Test
    @Ignore
    public void testGetAllUsers() {
        Assert.assertTrue(userDao.getAllUsers().size() > 0);
    }


    @Test
    @Ignore
    public void testGetUser() {
        User user = getLastUser();
        Assert.assertEquals(userDao.getUser(user.getId()).getId(), user.getId());
    }

    @Test
    @Ignore
    public void testEditUser() {
        User user = getLastUser();
        user.setName("updated");
        userDao.updateUser(user);
        Assert.assertEquals("updated", userDao.getUser(user.getId()).getName());
    }

    @Test
    @Ignore
    public void testDeleteUser() {
        int size = userDao.getAllUsers().size();
        User testUser = getLastUser();
        userDao.deleteUser(testUser.getId());

        Assert.assertTrue(userDao.getAllUsers().size() < size);
    }

    /**
     * gets all the users and returns the last added user
     *
     * @return : last added user
     */
    private User getLastUser() {
        List<User> user_list = userDao.getAllUsers();
        return user_list.get(user_list.size() - 1);
    }
}